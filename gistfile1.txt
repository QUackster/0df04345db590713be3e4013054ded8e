using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurniAlias
{
    class Program
    {
        private static string FFDEC_PATH = @"C:\Program Files (x86)\FFDec\ffdec.exe";
        private static string SWF_PATH = "swfs";

        static void Main(string[] args)
        {
            Console.Title = "FurniAlias";

            Console.WriteLine("Furniture renamer by Quackster - 2020");
            Console.WriteLine();

            if (!Directory.Exists(SWF_PATH))
                Directory.CreateDirectory(SWF_PATH);


            var newDirectory = Path.Combine("swfs_renamed");
            DeleteAndCreate(newDirectory);

            string exportDirectory = Path.Combine(SWF_PATH, "temp");

            foreach (var file in Directory.GetFiles(SWF_PATH, "*.swf"))
            {
                DeleteAndCreate(exportDirectory);

                string originalSWFFileName = Path.GetFileName(file);
                string currentSWFName = Path.GetFileNameWithoutExtension(file);
                string tempSWFPath = Path.Combine(exportDirectory, originalSWFFileName);
                string tempXMLPath = Path.Combine(exportDirectory, currentSWFName + ".xml");

                File.Copy(file, tempSWFPath);

                Console.Write($"Rename {currentSWFName} to => ");
                string newName = Console.ReadLine().Replace(".swf", "");

                SWF2XML(tempSWFPath, tempXMLPath);
                ReplaceWord(tempXMLPath, currentSWFName, newName);
                XML2SWF(tempXMLPath, tempSWFPath);

                ExtractAssets(exportDirectory, tempSWFPath);

                var binaryDataDirectory = Path.Combine(SWF_PATH, "temp", "binaryData");
                Replace(binaryDataDirectory, currentSWFName, newName);

                foreach (string binaryFile in Directory.GetFiles(binaryDataDirectory, "*", SearchOption.AllDirectories))
                {
                    if (ReplaceWord(binaryFile, currentSWFName, newName))
                    {
                        var p = new Process();
                        p.StartInfo.FileName = FFDEC_PATH;
                        p.StartInfo.Arguments = string.Format("-replace \"{0}\" \"{1}\" \"{2}\" \"{3}\"", tempSWFPath, tempSWFPath, Path.GetFileName(binaryFile).Split('_')[0], binaryFile);
                        p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                        p.StartInfo.CreateNoWindow = true;
                        p.Start();
                        p.WaitForExit();
                    }
                }


                File.Copy(tempSWFPath, Path.Combine(newDirectory, newName + ".swf"));
            }

            Console.WriteLine("Done!");
        }

        private static void DeleteAndCreate(string exportDirectory)
        {
            if (Directory.Exists(exportDirectory))
                Directory.Delete(exportDirectory, true);

            Directory.CreateDirectory(exportDirectory);
        }

        public static void Replace(string directory, string search, string newWord)
        {
            foreach (string file in Directory.GetFiles(directory, "*", SearchOption.AllDirectories))
            {
                ReplaceWord(file, search, newWord);
            }
        }

        public static bool ReplaceWord(string file, string find, string replaceWith)
        {
            /*string fileName = Path.GetFileName(file);
            string newFileName = fileName.Replace(find, replaceWith);
            string newFilePath = file.Replace(fileName, newFileName);

            File.Move(file, newFilePath);

            if (Path.GetExtension(file) == ".bin" || Path.GetExtension(file) == ".txt" || Path.GetExtension(file) == ".xml" || Path.GetExtension(file) == ".props" || Path.GetExtension(file) == ".data" || Path.GetExtension(file) == ".index")
            {
                string content = File.ReadAllText(newFilePath);
                content = content.Replace(find, replaceWith);
                File.WriteAllText(newFilePath, content);
            }*/

            if (Path.GetExtension(file) == ".bin" || 
                Path.GetExtension(file) == ".txt" || 
                Path.GetExtension(file) == ".xml" || 
                Path.GetExtension(file) == ".props" || 
                Path.GetExtension(file) == ".data" || 
                Path.GetExtension(file) == ".index")
            {
                string content = File.ReadAllText(file);

                if (content.Contains(find))
                {
                    content = content.Replace(find, replaceWith);
                    File.WriteAllText(file, content);
                    return true;
                }
            }

            return false;
        }

        private static void ExtractAssets(string outputPath, string fullFileName)
        {
            var p = new Process();
            p.StartInfo.FileName = FFDEC_PATH;
            p.StartInfo.Arguments = string.Format("-export \"all\" \"{0}\" \"{1}\"", outputPath, fullFileName);
            p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p.StartInfo.CreateNoWindow = true;
            p.Start();
            p.WaitForExit();
        }

        private static void SWF2XML(string inFile, string outFile)
        {
            var p = new Process();
            p.StartInfo.FileName = FFDEC_PATH;
            p.StartInfo.Arguments = string.Format("-swf2xml \"{0}\" \"{1}\"", inFile, outFile);
            p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p.StartInfo.CreateNoWindow = true;
            p.Start();
            p.WaitForExit();
        }

        private static void XML2SWF(string inFile, string outFile)
        {
            var p = new Process();
            p.StartInfo.FileName = FFDEC_PATH;
            p.StartInfo.Arguments = string.Format("-xml2swf \"{0}\" \"{1}\"", inFile, outFile);
            p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p.StartInfo.CreateNoWindow = true;
            p.Start();
            p.WaitForExit();
        }
    }
}
